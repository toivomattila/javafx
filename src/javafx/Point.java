/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafx;

import javafx.scene.shape.Circle;

/**
 *
 * @author Toivo
 */
public class Point {
    private Circle circle;
    private String name;

    public Point(Circle circle, String name) {
        this.circle = circle;
        this.name = name;
        System.out.println("Hei, olen piste!");
    }

    public Circle getCircle() {
        return circle;
    }

    public String getName() {
        return name;
    }

    public void setCircle(Circle circle) {
        this.circle = circle;
    }
}
