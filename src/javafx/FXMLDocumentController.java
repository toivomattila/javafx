/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafx;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;


/**
 *
 * @author Toivo
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private AnchorPane apMain;
    
    private ShapeHandler sh;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        sh = ShapeHandler.getInstance(apMain);
    }
}
