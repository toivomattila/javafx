/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafx;

import java.util.ArrayList;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 *
 * @author Toivo
 */
public class ShapeHandler {
    private static ShapeHandler sh = null;
    private ArrayList<Point> allPoints;
    private ArrayList<Line> allLines;
    private Point lineStart;

    private ShapeHandler(AnchorPane ap) {
        allPoints = new ArrayList<>();
        allLines = new ArrayList<>();
        lineStart = new Point(null, "LineStart");
        ap.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent me) -> {
            if(me.getButton().equals(MouseButton.PRIMARY)) {
                Circle temp = new Circle(me.getX(), me.getY(), 10, Color.BLUE);
                if(circleNotInMap(temp)){
                    addPoint(temp, "Piste", ap);
                }
            }
        });
    }
    
    static public ShapeHandler getInstance(AnchorPane ap){
        if(sh == null){
            sh = new ShapeHandler(ap);
        }
        return sh;
    }
    
    public void addPoint(Circle circle, String name, AnchorPane ap){
        Point temp = new Point(circle, name);
        allPoints.add(temp);
        ap.getChildren().add(temp.getCircle());
        
        circle.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent me) -> {
            if(me.getButton().equals(MouseButton.PRIMARY)){
                drawLine(circle, ap);
            }
            if(me.getButton().equals(MouseButton.SECONDARY)) {
                deletePoint(circle, ap);
            }
        });
    }
    
    public void deletePoint(Circle circle, AnchorPane ap){
        ap.getChildren().remove(circle);
        allPoints.remove(circle);
    }

    public ArrayList<Point> getAllPoints() {
        return allPoints;
    }
    
    private boolean circleNotInMap(Circle circle){
        for(int i = 0; i < allPoints.size(); i++){
            //mouse is inside the circle if abs(circle_coord-mouse_coord) < circle_radius
            if(Math.abs(allPoints.get(i).getCircle().getCenterX() - circle.getCenterX()) <
                allPoints.get(i).getCircle().getRadius() && 
               Math.abs(allPoints.get(i).getCircle().getCenterY() - circle.getCenterY()) <
                allPoints.get(i).getCircle().getRadius()){
                return false;
            }
        }
        return true;
    }
    
    private void drawLine(Circle circle, AnchorPane ap){
        if(lineStart.getCircle() != null){
            //Uncomment for only one line (11.4)
            if(allLines != null && !allLines.isEmpty()){
                ap.getChildren().remove(allLines.get(allLines.size()-1));
            }
            Line line = new Line(lineStart.getCircle().getCenterX(),
                                 lineStart.getCircle().getCenterY(),
                                 circle.getCenterX(),
                                 circle.getCenterY());
            allLines.add(line);
            ap.getChildren().add(line);
        }
        lineStart.setCircle(circle);
    }
}
